import React from 'react';
import ReactDOM from 'react-dom';
import { PortfolioApp } from './PortfolioApp';

import './styles/style.scss';


ReactDOM.render(
    <PortfolioApp/>,
  document.getElementById('root')
);

