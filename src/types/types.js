export const types ={
    // authReducer
    login: '[Auth] Login',
    logout: '[Auth] Logout',

    // uiReducer
    uiSetError: '[UI] Set Error ',
    uiRemoveError: '[UI] Remove Error ',
    
    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',

    // notes

    notesAddNew: '[Notes] New note',
    notesActive: '[Notes] Set active note',
    notesLoad: '[Notes] Load note',
    notesUpdated: '[Notes] Updated note',
    notesFileUrl: '[Notes] Updated img url',
    notesDelete: '[Notes] Delete note',
    notesLogoutCleaning: '[Notes] Logout Cleaning',

    // userData

    userDataAddNew: '[UserData] New userData',
    userDataActive: '[UserData] Set active note',
    userDataLoad: '[UserData] Load note',
    userDataUpdated: '[UserData] Updated note',
    userDataFileUrl: '[UserData] Updated img url',
    userDataDelete: '[UserData] Delete note',
    userDataLogoutCleaning: '[UserData] Logout Cleaning',


}