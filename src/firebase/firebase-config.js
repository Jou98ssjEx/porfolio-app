// import firebase from 'firebase/app';
// import 'firebase/firestore';
// import 'firebase/auth';

// v9 compat packages are API compatible with v8 code
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// configuracion dada x firebase para la web : registro de app
const firebaseConfig ={
    apiKey: "AIzaSyAiFEG8DFgfKv2-DNTygRYU0g1hEnYDMQ4",
    authDomain: "portfolioapp-9c3ef.firebaseapp.com",
    projectId: "portfolioapp-9c3ef",
    storageBucket: "portfolioapp-9c3ef.appspot.com",
    messagingSenderId: "268498757565",
    appId: "1:268498757565:web:0b4e6398ec34e5301b14d4",
    measurementId: "G-RLLNV6R7PF"
}


// v antigua
// firebase.initializeApp(firebaseConfig);
// const db = firebase.firestore();
// const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


firebase.initializeApp(firebaseConfig);
const db = firebase.firestore(); // lleva la app
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


export{
    db,
    googleAuthProvider,
    firebase
}