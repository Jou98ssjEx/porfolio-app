import React from 'react';
import {Provider} from 'react-redux';

import { AppRouters } from './routes/AppRouters';
import { store } from './store/store';

export const PortfolioApp = () => {
  return (
    <>
      {/* // Aqui va el provider (store) */}
      <Provider store={ store }>
        <AppRouters />
      </Provider>
    </>  
  );
};
