import { types } from "../types/types";

const initialState = {
    data: [],
    // dataUser: {},
    // social: [],
    active: null
}

export const userReducer = ( state = initialState, action ) => {
    switch (action.type) {
        case types.userDataAddNew:
            
            return {
                state
            };

        case types.userDataLoad:

            return {
                ...state,
                dataUser: action.payload
            }
    
        default:
            return state
    }
}