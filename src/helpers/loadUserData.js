import { db } from "../firebase/firebase-config"


export const loadUserData = async( uid ) => {

    // llama la collecion de las notes en ref al uid del user
    const notesSnap =  await db.collection(`${uid}/user/data`).get();
    // console.log(notesSnap);
    const dataUser = {};

    // // los elementos se encuentran en el snapChildren.data();
    notesSnap.forEach( snapChildren => {
        // console.log(snapChildren.data())
        // notes.push({
        //     id: snapChildren.id,
        //     ...snapChildren.data()
        // })
        dataUser.id = snapChildren.id;
        dataUser.data = snapChildren.data();
    })
    return dataUser;

    // // console.log(notes)
    // return notes;
}