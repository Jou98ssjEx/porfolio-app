import { db } from "../firebase/firebase-config"


export const loadNotes = async( uid ) => {

    // llama la collecion de las notes en ref al uid del user
    const notesSnap =  await db.collection(`${uid}/journal/notes`).get();
    // console.log(notesSnap);
    const notes = [ ];

    // los elementos se encuentran en el snapChildren.data();
    notesSnap.forEach( snapChildren => {
        // console.log(snapChildren.data())
        notes.push({
            id: snapChildren.id,
            ...snapChildren.data()
        })
    })

    // console.log(notes)
    return notes;
}