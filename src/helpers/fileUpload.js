
export const fileUpload = async(file) => {

    const urlCloudinary = 'https://api.cloudinary.com/v1_1/dgekgbspr/upload';
    const preset = 'react-journal';

    const formDate = new FormData();
    formDate.append('upload_preset', preset);
    formDate.append('file', file);
    // console.log(file);

    
    try {
        // peticion POST
    
        const resp = await fetch(urlCloudinary, {
            method: 'POST',
            body: formDate
        });

        const {secure_url} = await resp.json();
        // console.log( secure_url );
        return secure_url;
        
    } catch (error) {
        console.log(error);
    }
}