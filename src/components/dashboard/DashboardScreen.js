import React, { useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import { ContentScreen } from './ContentScreen';
import { EducationContent } from './education/EducationContent';
import { ExperenceContent } from './experence/ExperenceContent';
import { Footer } from './Footer';
import { Navbar } from './Navbar';
import { PortfolioContent } from './portfolio/PortfolioContent';
import { Siderbar } from './siderbar/Siderbar';
import { UserContent } from './user/UserContent';

export const DashboardScreen = () => {

  const [siderbarShow, setSiderbarShow] = useState(true);

  const siderbarHiden = () => {
      console.log(siderbarShow);
      setSiderbarShow(!siderbarShow);
  }

  return (
    
    <>
      {/* // Siderbar */}
      <aside 
        className={ siderbarShow ? 'dashboard__siderbar hidden-menu' : 'dashboard__siderbar show-menu'}
        // className='dashboard__siderbar'
        id='dash__siderbar'
        >
        <Siderbar />
      </aside>

      {/* // App Bar o header */}
      <header className={ siderbarShow ? 'body-container-nav': 'body-container'} id="header">
      {/* <header className="body-container-nav" id="header"> */}
        <div
            className="menu__toggle"
            id="menu-toggle"
            onClick={ siderbarHiden }
            >
            <i className='bx bx-menu' id="btn"></i>
        </div>
        <Navbar />
      </header>

      {/* // Main content */}
      {/* <ContentScreen /> */}
      {/* <div className={ siderbarShow ? 'main' : 'mainView'}> */}
      <div className={ siderbarShow ? 'main body-container-nav' : 'mainView body-container'}>
        <Routes>
          <Route path='/' element={ <UserContent />} />
          {/* <Route path='/' element={ <ContentScreen />} /> */}
          <Route path='/user' element={ <UserContent />} />
          <Route path='/portfolio' element={ <PortfolioContent />} />
          <Route path='/education' element={ <EducationContent />} />
          <Route path='/Experence' element={ <ExperenceContent />} />
        </Routes>
      </div>

      {/* //Footer */}
      <footer className={ siderbarShow ? 'body-container-nav': 'body-container'} id="footer">
        <Footer />
      </footer>
    </>
  );
};
