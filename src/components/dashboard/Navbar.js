import React from 'react';

export const Navbar = () => {
    // const [siderbarShow, setSiderbarShow] = useState(true);

    // const siderbarHiden = () => {
    //     console.log(siderbarShow);
    //     setSiderbarShow(!siderbarShow);
    // }
  return (
    <>
      {/* <header className={ siderbarShow ? 'body-container' : "body-container-nav"} id="header"> */}
      {/* <header className="body-container-nav" id="header">
        <div
            className="menu__toggle"
            id="menu-toggle"
            onClick={ siderbarHiden }
            >
            <i className='bx bx-menu' id="btn"></i>
        </div> */}

        <div className="header__icons">
            <i className='bx bx-search-alt'></i>
            <i className='bx bx-bookmark'></i>
            <i className='bx bx-message-detail'></i>
        </div>

        <div className="header__data">
            <div className="header__user">

                <div className="header__img">
                    <img src="/assets/img/Cv.png" alt=""/>
                </div>

                <div className="header__info">
                    <span><b> Jou G</b> </span>
                    <span>Super admin</span>
                </div>
                <i className='bx bx-moon'></i>

            </div>

        </div>
    {/* </header> */}
    </>
  );
};
