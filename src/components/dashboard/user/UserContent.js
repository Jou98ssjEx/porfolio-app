import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startDataUser } from '../../../actions/userData';
import { useForm } from '../../../hooks/useForm';
import { SocialEntries } from './SocialEntries';
import { SocialEntry } from './SocialEntry';

export const UserContent = () => {

  const dispatch = useDispatch();

  const { dataUser } = useSelector(state => state.userData);
  console.log({dataUser});

  // const initialState = {
  //   name: 'algo',
  //   profesion: 'profesion',
  //   profile: 'I have profile in the box',
  //   url: 'algo',
  //   social: [
  //     {
  //       name: 'Icon',
  //       icon: 'bx bx-leaf',
  //       url: '',
  //     }
  //   ]
  // }

  const [ formValues, handleInputChange, reset] =  useForm(dataUser);
  // console.log({formValues});

  const [values] = useState(formValues);
  
  const { name, profesion, profile, url, social } = values;
  console.log(name);

  // useEffect(() => {
  //   // console.log(name);
  // }, [formValues]);
  
  // const userIdRef = useRef(dataUser);
  // console.log(userIdRef);

  // useEffect(() => {
  //   if( dataUser.id !== userIdRef ){
  //     reset(dataUser)
  //     userIdRef.current = dataUser.id
  //   }
  // }, [reset, dataUser]);
  
  // dispatch(startDataUser());
  
  return (
    <section className='dashboard'>

        <div className='user__title'>
          <h1>Info user: Page Home of Portfolio</h1>

        </div>

        <div className='user__content'>
          <div className='user__data'>
            <div className='user__info'>
              <div className='user__block'>
                <label> Name: </label>
                <input
                  type='text'
                  placeholder='Name'
                  name='name'
                  value={ name }
                  className=''
                  autoComplete='off'
                  onChange={ handleInputChange }
                />
              </div>
              <div className='user__block'>
                <label> Profesion: </label>
                <input
                  type='text'
                  placeholder='Profesion'
                  name='profesion'
                  value={ profesion }
                  className=''
                  autoComplete='off'
                  onChange={ handleInputChange }
                />
              </div>
            </div>
            <div className='user__info'>
              <div className='user__block'>
                <label> Introduction: </label>
                <textarea
                  type='text'
                  placeholder='Introduction'
                  name='profile'
                  value={ profile }
                  className=''
                  onChange={ handleInputChange }
                />

              </div>
            </div>

            <button
            className='btn'
              
            >
              Save
            </button>

          </div>

          <div className='user__img'>
            <label>Picture</label>
            {
              // dataUser.url && 
              // (
              //   <div>
              //     <img
              //       src={dataUser.url}
              //       // src='/assets/img/Cv.png'
              //     />

              //   </div>

              // )
            }
            <div>
                  <img
                    // src={dataUser.url}
                    src='/assets/img/Cv.png'
                  />

                </div>

            <button
                className='btn'
            >Upload</button>

          </div>

        </div>
        
        <div>
          <div className='user__social__title'>
            <h2>Social</h2>
            {/* <hr/> */}
          </div>

          <div className='user__social_content'>
            <div>
              <div
                  className='user__social__entry'
                  // onClick={handleNewNote}
              >
                  <i className='far fa-calendar-plus ' ></i>
                  <p className=''>New entry</p>
              </div>
              <div className='user__social__entries'>
                <SocialEntries />
              </div>
            </div>
            <div>
              <SocialEntry />
            </div>

          </div>
        </div>

     

    </section>
  );
};
