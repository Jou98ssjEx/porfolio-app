import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startLogout } from '../../actions/auth';

export const Siderbar = () => {

    const { name } = useSelector(state => state.auth);

    const dispatch = useDispatch();
    
    const handleLogout = ( ) => {
        // console.log('click')
        dispatch(startLogout());
    }

  return (
    <div className='sidebar' id='sidebar'>

    <div className='sidebar__brand'>
        <i className='bx bxs-dashboard icon__sidebar'></i>
        <div className='sidebar__admin'>
            <span>
                Dashboard
            </span>
        </div>
    </div>

    <div className='sidebar__menu'>
        <ul className='nav__list'>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-user icon__sidebar-menu'></i>
                    <span className='links__name'>User</span>
                </a>
                <span className='tooltip'>User</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bxs-school icon__sidebar-menu'></i>
                    <span className='links__name'>Education</span>
                </a>
                <span className='tooltip'>Education</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-task icon__sidebar-menu'></i>
                    <span className='links__name'>Skills</span>
                </a>
                <span className='tooltip'>Skills</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bxs-briefcase icon__sidebar-menu'></i>
                    <span className='links__name'>Experence</span>
                </a>
                <span className='tooltip'>Experence</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-certification icon__sidebar-menu'></i>
                    <span className='links__name'>Certificate</span>
                </a>
                <span className='tooltip'>Certificate</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-planet icon__sidebar-menu'></i>
                    <span className='links__name'>Language</span>
                </a>
                <span className='tooltip'>Language</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-layer-minus icon__sidebar-menu'></i>
                    <span className='links__name'>Interests</span>
                </a>
                <span className='tooltip'>Interests</span>
            </li>
            <li className='nav__list-li'>
                <a className='nav__list-a' href='#'>
                    <i className='bx bx-cog icon__sidebar-menu'></i>
                    <span className='links__name'>Setting</span>
                </a>
                <span className='tooltip'>Setting</span>
            </li>
        </ul>
    </div>

    <div className='sidebar__footer'>
        <div className='sidebar__footer-data'>
            <span>Login as: <b> { name } </b></span>
            <i onClick={ handleLogout } className='bx bx-log-out icon__sidebar-footer' id='log_out'></i>
        </div>
        <div className='sidebar__footer-time'>
            <span>Fecha</span>
            <span>Hora</span>
        </div>
    </div>
</div>
    );
};
