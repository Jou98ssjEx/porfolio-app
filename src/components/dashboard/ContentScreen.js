import React from 'react';

export const ContentScreen = () => {
  return (

    // <section>
      <div className="dash__header">
            <div className="dash__info">
                <h1>Analytics Dahboard</h1>
                <small>Monitor key metric. Check reporting and review insights</small>
            </div>
            <div className="dash__action">
                <button>
                    <i className='bx bx-download icons__dash' ></i> 
                    <span>
                        Download
                    </span>
                </button>
            </div>
        </div>

    // </section>
  );
};
