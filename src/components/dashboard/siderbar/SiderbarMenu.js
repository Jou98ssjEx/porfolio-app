import React from 'react';
import { Link, NavLink } from 'react-router-dom';

export const SiderbarMenu = () => {

    const data = [
        
        {
            name: 'User',
            icon: 'bx bx-user icon__sidebar-menu',
            route: '/dashboard/user',
        },
        
        {
            name: 'Education',
            icon: 'bx bxs-school icon__sidebar-menu',
            route: '/dashboard/education',
        },
        {
            name: 'Skills',
            icon: 'bx bx-task icon__sidebar-menu',
            route: '/dashboard/skills',
        },
        {
            name: 'Experence',
            icon: 'bx bxs-briefcase icon__sidebar-menu',
            route: '/dashboard/experence',
        },
        {
            name: 'Certificate',
            icon: 'bx bx-certification icon__sidebar-menu',
            route: '/dashboard/certificate',
        },
       
        {
            name: 'Language',
            icon: 'bx bx-planet icon__sidebar-menu',
            route: '/dashboard/language',
        },
        {
            name: 'Interests',
            icon: 'bx bx-layer-minus icon__sidebar-menu',
            route: '/dashboard/interests',
        },
        {
            name: 'Setting',
            icon: 'bx bx-cog icon__sidebar-menu',
            route: '/dashboard/setting',
        },
    ]
    return (
    <>
        <div className="sidebar__menu">
            <ul className="nav__list">

                {
                    data.map( ( m, i ) => (
                        
                        <li 
                            className="nav__list-li"
                            key={ i }
                        >
                            <NavLink 
                                className="nav__list-a"
                                to={ m.route }
                            >
                                <i 
                                    className={ m.icon }
                                ></i>
                                <span className="links__name">{ m.name }</span>
                            </NavLink>
                            <span className="tooltip"> {m.name} </span>
                        </li>
                    ))
                }

                
            </ul>
        </div>
    </>
    );
};
