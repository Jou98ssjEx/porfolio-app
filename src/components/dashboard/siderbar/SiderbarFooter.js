import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startLogout } from '../../../actions/auth';

export const SiderbarFooter = () => {

    const { name } = useSelector(state => state.auth);

    const dispatch = useDispatch();
    
    const handleLogout = ( ) => {
        // console.log('click')
        dispatch(startLogout());
    }
    return (
    <>
        <div className="sidebar__footer">
            <div className="sidebar__footer-data">
                <span>Login as: <b> { name }</b></span>
                <i
                    onClick={ handleLogout }
                 className='bx bx-log-out icon__sidebar-footer' id="log_out"></i>

            </div>
            <div className="sidebar__footer-time">
                <span>Fecha</span>
                <span>Hora</span>
            </div>
        </div>  
    </>
    );
};
