import React from 'react';
import { useNavigate } from 'react-router-dom';

export const SiderbarBrand = () => {

  const navigate = useNavigate();

  
  const handleMain = () => {
    // navegar al mainScreen
    navigate('/dashboard', {replace: true});

  }

  return (
  <>
    <div
      className="sidebar__brand pointer"
      onClick={ handleMain }
    >
        <i className='bx bxs-dashboard icon__sidebar'></i>
        <div className="sidebar__admin">
            <span>
                Dashboard
            </span>
        </div>
    </div>
  </>
  );
};
