import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { startLogout } from '../../../actions/auth';
import { SiderbarBrand } from './SiderbarBrand';
import { SiderbarFooter } from './SiderbarFooter';
import { SiderbarMenu } from './SiderbarMenu';

export const Siderbar = () => {

    const { name } = useSelector(state => state.auth);

    const dispatch = useDispatch();
    
    const handleLogout = ( ) => {
        // console.log('click')
        dispatch(startLogout());
    }

  return (
  //  <aside 
  //   className='dashboard__siderbar'
  //   id='dash__siderbar'
  //   >
      <>
       {/* // cabecera de dashboard */}
       <SiderbarBrand />

       {/* // Menus del dash */}
       <SiderbarMenu />

       {/* // footer del dash */}
       <SiderbarFooter />   
       </>
  //  </aside>
  );
};
