import React from 'react';
import { Link, Navigate } from 'react-router-dom';

export const Main = () => {

  const dataLinks =[
    {
      name: 'Login',
      route: '/auth/login',
      icon: 'bx bxs-user-account',
      // icon: 'uil uil-signin',
      // icon: 'fab fa-html5',
      color: 'main__card main__c1'
    },
    {
      name: 'Cv',
      route: '/cv',
      // icon: 'uil uil-book-reader',
      icon: 'bx bx-spreadsheet',
      // icon: 'fab fa-html5',
      color: 'main__card main__c2'
    },
    {
      name: 'Portfolio',
      route: '/portfolio',
      icon: 'bx bx-book-content',
      // icon: 'uil uil-bag',
      // icon: 'fab fa-html5',
      color: 'main__card main__c3'
    },
   ] 


  return (
    <div className='main__bg'>
      <div className='main__content'>
        <h1>Welcome</h1>

        <div className='main__container__card'>
            {
              dataLinks.map( (m, i) => (
                <Link 
                  className={m.color}
                  key={i}
                  to={m.route}
                >
                  <div 
                    className='main__icon'
                  >
                    <i className={m.icon}></i>
                  </div>

                  <div className='main__title'> 
                    <p> {m.name} </p>
                  </div>

                </Link>
                
              ))
            }
        </div>
      </div>
     
    </div>
        
  );
};
