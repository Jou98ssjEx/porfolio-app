import React from 'react';
import validator from 'validator';

import { Link } from 'react-router-dom';
import { useForm } from '../../hooks/useForm';
import { useDispatch, useSelector } from 'react-redux';
import { startGoogleLogin, startLoginEmailPassword } from '../../actions/auth';
import { removeError, setError } from '../../actions/ui';

export const LoginScreen = () => {

  const dispatch = useDispatch();

  // useSelector
  // consigo el stado de: ui
  const { msgError, logged } = useSelector(state => state.ui);

  const initialState = {
    // name: 'Barry Allen F',
    email: '',
    password: ''
    // email: 'flash98@gmail.com',
    // password: '123456'
  }

  const [ { email ,  password }, hadleChangeInput, reset ] = useForm( initialState );

  const handleSumitLogin = ( e ) => {
    e.preventDefault()

    if( isFormValid() ) {
      // accion;
      dispatch( startLoginEmailPassword(email, password));
    }
    reset();
    // console.log('click');
  }

  const handleGoogleLogin = () => {

    dispatch( startGoogleLogin() );

  }

  // validacion de los campos del formulario
  const isFormValid = () => {

   if (!validator.isEmail(email)){
        // console.log('Email incorrect')
        dispatch( setError('Email incorrect') );
        return false;

    } else if ( password.length < 4){
        // console.log('Error in password')
        dispatch( setError('Error in password') );
        return false
    }

    dispatch( removeError());
    
    return true
}

  return (
    <div className='auth__container'>

      <div className='auth__forms-container'>

        <div className='auth__signin'>

          <form 
            onSubmit={ handleSumitLogin } 
            className='auth__sign-in-form'
          >

              <h2 className='auth__title'>Sign in</h2>

              {
                // Mesajes de errores de los campos
                  msgError && (
                      <div className='auth__alert-error'>
                          {msgError}
                      </div>  
                  )
              }

              <div className='auth__input-field'>
                <i className='fas fa-user'></i>
                <input 
                  type='text'
                  placeholder='Username' 
                  name='email'
                  value={ email }
                  autoComplete='off'
                  autoFocus='on'
                  onChange={ hadleChangeInput }
                  placeholder='email@live.com'
                />
              </div>

              <div className='auth__input-field'>
                <i className='fas fa-lock'></i>
                <input 
                  type='password' 
                  placeholder='Password' 
                  name='password'
                  value={ password }
                  autoComplete='off'
                  onChange={ hadleChangeInput }
                  placeholder='password'
                />
              </div>

              <input 
                type='submit' 
                value='Login' 
                className='auth__btn solid'
                disabled={ logged }
              />

              <p className='auth__social-text'>Login with social networks or return</p>

              <div className='auth__social-media'>

              <div
                onClick={ handleGoogleLogin }
                className='pointer'
              >
                <a className='auth__social-icon'>
                    <i className='fab fa-google'></i>
                </a>
              </div>

              <div>
                <Link className='auth__social-icon' to='/'>
                    <i className='bx bx-log-out'></i>
                </Link>
              </div>

                  
              </div>
            </form>
      
        </div>

      </div>

      <div className='panels-container'>
        <div className='panel left-panel'>
          <div className='content'>
            <h3>New here ?</h3>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis,
              ex ratione. Aliquid!
            </p>

            <Link 
              className='auth__btn__register transparent'
              to='/auth/register'
              id='sign-up-btn'
            >
              Sign up
            </Link>


          <Link 
            className="auth__btn__register transparent" 
            id="sign-in-btn"
            to='/'
            >
            Main
          </Link>
          </div>
          <img src='/assets/img/log.svg' className='image' alt='' />
        </div>

      </div>

    </div>
    
  );
};
