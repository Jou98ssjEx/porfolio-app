import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import validator from 'validator';

import { startGoogleLogin, startRegisterNameEmailPassword } from '../../actions/auth';
import { removeError, setError } from '../../actions/ui';
import { useForm } from '../../hooks/useForm';

export const RegisterScreen = () => {

  // acciones de los reducer
  const dispatch = useDispatch();

  // consigo el stado del los errores: ui
  const { msgError } = useSelector(state => state.ui);

  const initialState = {
    name: '',
    email: '',
    password: '',
    confirm: '', 
    // name: 'Barry Alen',
    // email: 'flash98@live.com',
    // password: '123456',
    // confirm: '123456', 
  }

  // uso de useForm para el formulario
  const [formValues, hadleChangeInput, reset] = useForm( initialState );

  const { name, email, password, confirm } = formValues;

  // envio de la data a firebase
  const handlerSumit = ( e ) => {
    e.preventDefault();

    if ( isFormValid() ){
      dispatch(startRegisterNameEmailPassword(name, email, password));
      // console.log('Form successfy');
      reset();
    }

  }

  // auth google
  const handleGoogleLogin = () => {

    dispatch( startGoogleLogin() );

  }

  // validacion de los campos del formulario
  const isFormValid = () => {

    if( name.trim().length === 0){
        // console.log('Name is required')
        dispatch( setError('Name is required') );
        return false
    } else if (!validator.isEmail(email)){
        // console.log('Email incorrect')
        dispatch( setError('Email incorrect') );
        return false
    } else if ( password !== confirm || password.length < 4){
        // console.log('Error in password')
        dispatch( setError('Error in password') );
        return false
    }

    dispatch( removeError());
    
    return true
}

  return (
    <div className='auth__container__register'>
    
    <div className='auth__forms-container'>
      
      <div className='auth__signup'>

        <form 
          className='auth__sign-up-form'
          onSubmit={ handlerSumit }
          >

          <h2 className='auth__title'>Sign up</h2>

          {
            // Mesajes de errores de los campos
              msgError && (
                  <div className='auth__alert-error'>
                      {msgError}
                  </div>  
              )
          }

          <div className='auth__input-field'>
            <i className='fas fa-user'></i>
            <input 
              type='text' 
              placeholder='Username'
              name='name' 
              onChange={ hadleChangeInput }
              value={name}
              autoFocus='on'
              autoComplete='off'
            />
          </div>

          <div className='auth__input-field'>
            <i className='fas fa-envelope'></i>
            <input 
              type='email' 
              placeholder='Email' 
              name='email'
              onChange={ hadleChangeInput }
              value={email}
              autoComplete='off'
            />
          </div>

          <div className='auth__input-field'>
            <i className='fas fa-lock'></i>
            <input 
              type='password' 
              placeholder='Password'
              name='password' 
              onChange={ hadleChangeInput }
              value={password}
              autoComplete='off'
            />
          </div>

          <div className='auth__input-field'>
            <i className='fas fa-lock'></i>
            <input 
              type='password' 
              placeholder='Confirm'
              name='confirm' 
              onChange={ hadleChangeInput }
              value={confirm}
              autoComplete='off'
            />
          </div>

          <input 
            type='submit' 
            className="auth__btn" 
            value="Sign up" 
            />
          
          <p className='auth__social-text'>Login with social networks</p>
          
          <div className='auth__social-media'>
            <div
              onClick={ handleGoogleLogin }
              className='pointer'
            >
              <a className='auth__social-icon'>
                  <i className='fab fa-google'></i>
              </a>
            </div>
          </div>

        </form>
      </div>

    </div>

    <div className="panels-container">
     
      <div className="panel right-panel">
        <div className="content">
          <h3>One of us ?</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum
            laboriosam ad deleniti.
          </p>
          <Link 
            className="auth__btn__register transparent" 
            id="sign-in-btn"
            to='/auth/login'
            >
            Sign in
          </Link>
         
        </div>
        <img src="/assets/img/register.svg" className="image" alt="" />
      </div>

      
    </div>

  </div>
  );
};
