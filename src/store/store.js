import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk'

import { authReducer } from '../reducers/authReducer';
import { uiReducer } from '../reducers/uiReducer';
import { userReducer } from '../reducers/userReducer';
// import { noteReducer } from '../reducers/noteReducer';

// configuracion de las devTools de redux
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;


// aqui add cualquier reducer
const reducers = combineReducers({
    auth: authReducer,
    ui: uiReducer,
    userData: userReducer,
    // notes: noteReducer,
});

// el corazon de redux: el stare
export const store = createStore(
    reducers,
    composeEnhancers(
        // aplicar middleware para las acciones asincronas
        applyMiddleware(thunk)
    )
);