import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { DashboardRoutes } from './DashboardRoutes';
import { PrivateRoutes } from './PrivateRoutes';
import { PublicRoutes } from './PublicRoutes';
import { MainRoutes } from './MainRoutes';
import {firebase} from '../firebase/firebase-config';
import { login } from '../actions/auth';
import { startLoadingDataUser } from '../actions/userData';
;


export const AppRouters = () => {

  const dispatch = useDispatch();

  const [cheking, setCheking] = useState(true);

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // console.log(isLoggedIn)

  useEffect(() => {
      
      // Mantener la authenticacion del usuario
      firebase.auth().onAuthStateChanged(async (user) => {
          // console.log(user)
          if (user?.uid){
              dispatch(login(user.uid, user.displayName))

              // saber si estoy logeado
              setIsLoggedIn(true);

              // // cargar notas del usuario
              // const notes =  await loadNotes(user.uid);

              dispatch(startLoadingDataUser(user.uid));
          } else{
              setIsLoggedIn(false);
          }
      })

      setCheking(false);
      
  }, [dispatch, setCheking, setIsLoggedIn])

  // en esta parte puede ser un componenete de loading
  if(cheking){
      return(
          <h1>Waiting....</h1>
      )
  }

  return (
    <BrowserRouter>
      <Routes>
          <Route path='/*' element={ 
              <PublicRoutes isLoggedIn={ isLoggedIn } >
                <MainRoutes/>
              </PublicRoutes>
          }/>

          <Route path='/dashboard/*' element={ 
              <PrivateRoutes isLoggedIn={ isLoggedIn } >
                <DashboardRoutes /> 
              </PrivateRoutes>
          }/>


          {/* <Route path='/' element={ <Main /> }/> */}

          
          {/* <Route path='/auth/*' element={ 
            <PublicRoutes>
              <AuthRoute/>
            </PublicRoutes>
          }/> */}

          {/* <Route path='/auth/*' element={ <AuthRoute /> }/>
        
          <Route path='/cv' element={ <CvScreen /> }/>
          <Route path='/portfolio' element={ <PortfolioScreen /> }/> */}

         

      </Routes>
    </BrowserRouter>
 );
};
