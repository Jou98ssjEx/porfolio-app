import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { DashboardScreen } from '../components/dashboard/DashboardScreen';

export const DashboardRoutes = () => {
  return (
    <div className='dashboard__main'>
        <Routes>
          <Route path='/*' element={ <DashboardScreen /> }/>
          {/* <Route path='/user' element={ <DashboardScreen /> }/>
          <Route path='/portfolio' element={ <DashboardScreen /> }/> */}

        </Routes>
    </div>
  );
};
