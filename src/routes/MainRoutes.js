import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { CvScreen } from '../components/cv/CvScreen';
import { Main } from '../components/Main';
import { PortfolioScreen } from '../components/portfolio/PortfolioScreen';
import { AuthRoute } from './AuthRoute';

export const MainRoutes = () => {
  return (
    <>

        <Routes>
            <Route path='/' element={ <Main /> }/>
            <Route path='/cv' element={ <CvScreen /> } />
            <Route path='/portfolio' element={ <PortfolioScreen /> } />
            <Route path='/auth/*' element={ <AuthRoute/> }/>
        </Routes>

    </>
);
};

