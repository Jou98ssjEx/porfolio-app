// import Swal from 'sweetalert2'
// import { db } from "../firebase/firebase-config";
// import { fileUpload } from '../helpers/fileUpload';
// import { loadNotes } from "../helpers/loadNotes";
// import { types } from "../types/types";


// export const startAddNewNote = () => {
//     return async( dispatch, getState) => {

//         // Obtener el state: uid for user
//         const {uid} = getState().auth;
//         // console.log(uid)

//         const newNote ={
//             title: '',
//             body: '',
//             // url: '',
//             date: new Date().getTime()
//         }

//         // asercion a db de firebase
//         const docRef = await db.collection(`${uid}/journal/notes`).add(newNote);
//         // si sale error de los permisos, ir a la ruler de firebase: poner true
//         // console.log(docRef)

//         dispatch( activeNote(docRef.id, newNote));

//         // muestra la nota en el siderbar
//         dispatch( addNewNote(docRef.id, newNote))
//         // dispatch( activeNote(uid, newNote));

//         // // muestra la nota en el siderbar
//         // dispatch( addNewNote(uid, newNote))

//     }
// }

// export const activeNote = ( id, note ) =>({
//     type: types.notesActive,
//     payload: {
//         id,
//         ...note
//     }
// })

// export const addNewNote = (id, note ) => ({
//     type: types.notesAddNew,
//     payload: {
//         id,
//         ...note
//     }
// })


// export const startLoadingNotes = ( uid ) =>{ 
//     return async (dispatch ) => {
    
//     // cargar notas del usuario
//     const notes =  await loadNotes(uid);

//     dispatch(setNotes(notes));

// }}


// // almacenar en el store
// export const setNotes = (notes) => ({
//     type: types.notesLoad,
//     payload: notes
// })

// // guardar la nota actualizada

// export const startSaveNote = (note) => {
//     return async ( dispatch, getState ) => {
//         // extraemos el id del usuario, esto del reducer
//         const { uid } = getState().auth;

//         // para los campos vacio: url eliminar
//         if (!note.url){
//             delete note.url
//         }

//         // creamos la nota actualizada eliminando el id de l a nota
//         const noteToFirestore = { ...note };
//         delete noteToFirestore.id;

//         // peticion de guardar a firestor
//         await db.doc(`${uid}/journal/notes/${note.id}`).update(noteToFirestore);

//         // hacer el refresh de la nota cambiada
//         dispatch(refreshNote(note.id, note))

//         Swal.fire('Updating', 'Date updating', 'success')
//     }
// }


// export const refreshNote = ( id, note ) => ({
//     type: types.notesUpdated,
//     payload: {
//         id, note
//         // se puede producir un error por el id de la nota, en este caso no
//     }
// })

// export const startFileUpload = ( file ) => {
//     return async ( dispatch, getState ) => {
//         // console.log(getState().notes);
//         const { active:activeNote } = getState().notes;

//         Swal.fire({
//             title: 'Auto close alert!',
//             text: 'I will close in 2 seconds.',
//             // timer: 2000
//             allowOutsideClick: false,
//             didOpen: () => {
//                 Swal.showLoading();
//             },
//             // onBeforeOpen: () => {
//             //     Swal.showLoading();
//             // } 
//         })
//         // obtener el url de la img subida a cloudynary
//         const urlCloud = await fileUpload(file);
//         // console.log(urlCloud);
//         activeNote.url = urlCloud;
//         // console.log(activeNote);
//         dispatch( startSaveNote(activeNote))
//         Swal.close();
//     }
// }

// export const startDeleteNote = ( id ) => {

//     return async (dispatch, getState) => {

//         const { uid } = getState().auth;
//         await db.doc(`${uid}/journal/notes/${id}`).delete();

//         dispatch( deleteNote(id));
//     }
// }

// export const deleteNote = ( id ) => ({
//     type: types.notesDelete,
//     payload: id
// })

// export const logoutCleaningNote = ( ) => ({
//     type: types.notesLogoutCleaning,
// })