import { db } from "../firebase/firebase-config";
import { loadUserData } from "../helpers/loadUserData";
import { types } from "../types/types";


export const startDataUser = ( ) => {

    return async( dispatch, getState) => {

        // Obtener el state: uid for user
        const {uid} = getState().auth;
        console.log(uid);

        const dataUser ={
            name: '',
            profesion: '',
            profile: '',
            url: '',
            date: new Date().getTime(),
            socialNetwork: [
                {
                    icon: '',
                    name: '',
                    url: '',
                }
            ]
        }

        // // asercion a db de firebase
        const docRef = await db.collection(`${uid}/user/data`).add(dataUser);
        // // si sale error de los permisos, ir a la ruler de firebase: poner true
        console.log(docRef);

        // dispatch( activeNote(docRef.id, newNote));

        // // muestra la nota en el siderbar
        // dispatch( addNewNote(docRef.id, newNote))
        // // dispatch( activeNote(uid, newNote));

        // // // muestra la nota en el siderbar
        // // dispatch( addNewNote(uid, newNote))

    }

}

export const startLoadingDataUser = ( uid ) =>{ 
    return async (dispatch ) => {
    
    // cargar notas del usuario
    const {data} =  await loadUserData(uid);
    // console.log(data);

    dispatch(setUserData(data));

}}


// almacenar en el store
export const setUserData = (data) => ({
    type: types.userDataLoad,
    payload: data
})