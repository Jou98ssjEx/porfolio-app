import { useState } from 'react'

export const useForm = (initialState ={}) => {

    console.log({initialState});

    const [form, setForm] = useState(initialState);

    const reset = (newState = initialState) =>{
        setForm(newState);
    }

    const hadleChangeInput = ( { target } ) => {
        // if( target.value.length < 1){
        //     console.log('0')
        //     throw new Error(`${target.name} is required`)
        // }
        setForm( {
            ...form,
            [target.name] : target.value
        })
    }

    return [form, hadleChangeInput, reset]
}
